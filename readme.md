### TV Room
![](https://bleedingcool.com/wp-content/uploads/2019/10/530524-watchmen-1200x900.jpg)
#### Goal of the project
Hi nerds! This project is for collecting, cleaning, transforming and visualising data from various sources.
For now it supports the GDELT database for collecting articles from the GKG database. Im planning to add more
datasources in the future(such as financial data like crypto prices, sentiments etc)
For now the only way to use the data is via jupyther notebooks but if Im not lazy I will add a simple
dashboard in the near future.

#### Installation

* create a new virtual environment at the project root:
```python3 -m venv ./```
* install the required libraries using pip:
```pip install -r requirements.txt```
* add the virtual environment as a new jupyter kernel:
```python -m ipykernel install --name=tv_room```
* check the generated kernel.json(the last command should print out the path), it should look like this:
```
{
 "argv": [
  "path-to-project-root/venv/bin/python",
  "-m",
  "ipykernel_launcher",
  "-f",
  "{connection_file}"
 ],
 "display_name": "tv_room",
 "language": "python"
}
```
* start the jupyter notebook server:
```jupyter notebook ./```
* navigate to the notebooks/ directory and open load_gkg_for_yesterday.ipynb Make sure that you use the tv_room kernel!
* run the script and wait patiently.
* Open notebooks/load_clean_gkg_data.ipynb and explore the data! Run the cells and have fun :)

### Future plans
* Dashboard
* More datasources
* More GDELT tables
* Alerts
* Modules for trading engines
* Airflow integration maybe
