
## Resources

https://docs.preqin.com/reports/Preqin-Special-Report-PME-July-2015.pdf
https://towardsdatascience.com/build-your-own-data-dashboard-93e4848a0dcf
https://nbviewer.jupyter.org/github/kdboller/pythonsp500/blob/b45eb79daf15d3c3032a8f398c207c9d2721ac19/Investment%20Portfolio%20Python%20Notebook_03_2018_blog%20example.ipynb
https://towardsdatascience.com/auto-generated-faq-with-python-dash-text-analysis-and-reddit-api-90fb66a86633
https://pypi.org/project/finta/
https://github.com/kylejusticemagnuson/pyti
https://coinlib.io/apidocs
https://blog.patricktriest.com/analyzing-cryptocurrencies-python/
https://www.gdeltproject.org/data.html
https://medium.com/@ImagineTraffic/27-amazing-resources-for-bitcoin-and-cryptocurrency-traders-911a7e92d918
https://www.coingecko.com/
https://financial-hacker.com/
https://www.altcointrading.net/strategy/wyckoff-ranging-markets/
https://www.reddit.com/user/merlin560
https://www.reddit.com/user/joyrider5
https://www.quantopian.com/posts/volume-spread-analysis-implementation
https://tradingsetupsreview.com/guide-volume-spread-analysis-vsa/
https://forexmentoronline.com/introduction-into-orderflow-trading/
https://financial-hacker.com/build-better-strategies/
https://financial-hacker.com/the-cold-blood-index/

https://wolfstreet.com/
https://finpedia.co/bin/Main/
## Useful commands
pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_columns', None)

## Modules
### Data module

* Collects ticker and other kinds of data
* Stores data
* Manipulates/transforms data (pivots, joins etc.)
* Serves stored data to other modules

### Quant module
* Applies calculations to data retrieved from the data module
* Prediction, diff, benchmarks etc.
* Caches results of calculations(optional)

### Viz module
* Draws vizualisations from data or results of calculations
* Provides dashboarding capabilities
* Metrics like
   * VWAP
   * PME
   * Industry Benchmarks

### Strategy module
* Trading strategies
* Backtests for every trading strategy

### Watcher
* Watches new data and pushes alerts when triggered by specific conditions(Strategy obj. gave a signal, stop loss hit etc.)
* Makes actions when triggered

### Trader
* Provides interfaces to trading APIs

### TODO
* unit tests
* version control
* actually implementing all this shit
