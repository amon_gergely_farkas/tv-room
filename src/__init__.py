import os
from logging import DEBUG, INFO

import logzero
from logzero import logger

from src.helpers import create_dir_if_not_exists

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

LOG_DIR = os.path.join(ROOT_DIR, "logs")
create_dir_if_not_exists(LOG_DIR)
logzero.logfile(os.path.join(LOG_DIR, "tv_room.log"), maxBytes=1e6, backupCount=3)
logger.info("TV Room Started!")
logzero.loglevel(INFO)
# logzero.loglevel(DEBUG)
