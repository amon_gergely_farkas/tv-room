import datetime as dt
import os
import re
from functools import partial
from multiprocessing import Pool, cpu_count

from logzero import logger

from src.etl.extractors import EXTRACTED_DATA_DIR
from src.helpers import create_dirs_if_not_exists, save_remote_file, default_date_range

TABLE_NAMES = ("export", "mentions", "gkg")

# Set up directories for GDELT data
# Each GDELT table gets its own directory
GDELT_DATA_DIR = os.path.join(EXTRACTED_DATA_DIR, "gdelt")
EXTRACTED_TABLES_DIR = os.path.join(GDELT_DATA_DIR, "extracted_tables")
EXTRACTED_TABLES_DIRS = {table_name: os.path.join(EXTRACTED_TABLES_DIR, table_name) for table_name in TABLE_NAMES}
HEADERS_DIR = os.path.join(GDELT_DATA_DIR, "headers")
create_dirs_if_not_exists(GDELT_DATA_DIR, HEADERS_DIR, *EXTRACTED_TABLES_DIRS.values())

MASTERFILE_PATH = os.path.join(GDELT_DATA_DIR, "masterfile.txt")
CAMEO_CODES_PATH = os.path.join(GDELT_DATA_DIR, "cameo_codefile.txt")
GCAM_CODES_PATH = os.path.join(GDELT_DATA_DIR, "gcam_codefile.txt")

MASTERFILE_URL = "http://data.gdeltproject.org/gdeltv2/masterfilelist.txt"
CAMEO_CODES_URL = "http://eventdata.parusanalytics.com/cameo.dir/CAMEO.SCALE.txt"
GCAM_CODES_URL = "http://data.gdeltproject.org/documentation/GCAM-MASTER-CODEBOOK.TXT"
HEADERS_URL = "https://raw.githubusercontent.com/linwoodc3/gdeltPyR/master/utils/schema_csvs/"

GDELT_CSV_FILENAME_REGEX = re.compile(
    r"(?:http|https)://data\.gdeltproject\.org/gdeltv2/(\d*)\.(\w*)\.(?:CSV|csv)\.zip")
GDELT_FILE_DATE_FORMAT = r"%Y%m%d%H%M%S"


@default_date_range()
def extract_gdelt_csvs(start_date=None, end_date=None, update=False):
    logger.info(f"Collecting GDELT data from {start_date} to {end_date} into {EXTRACTED_TABLES_DIR}")
    masterfile_path = save_remote_file(MASTERFILE_URL, MASTERFILE_PATH, override_old=True)
    gdelt_files = []
    with open(masterfile_path, "r") as master_file:
        lines = master_file.read()
    for ln in lines.split("\n"):
        # A column looks like this:
        #  263793 e23ee65a60a1577dc74b979a54da406e http://data.gdeltproject.org/gdeltv2/20150219001500.mentions.CSV.zip
        cols = ln.split(" ")
        if len(cols) != 3:
            continue
        _, checksum, url = cols
        date_str, table_name = GDELT_CSV_FILENAME_REGEX.findall(url)[0]
        date = dt.datetime.strptime(date_str, GDELT_FILE_DATE_FORMAT)
        if start_date <= date <= end_date:
            table_dir = EXTRACTED_TABLES_DIRS.get(table_name)
            if not table_dir:
                logger.warning(f"Unknown table :{table_name} extracted from {ln}")
                continue
            gdelt_files.append(
                (url, os.path.join(table_dir, f"{date_str}.csv.zip"))
            )

    with Pool(cpu_count()) as pool:
        saved_files = pool.starmap(partial(save_remote_file, override_old=update), gdelt_files)
    logger.info(
        f"Saved {len(saved_files)} GDELT files from {start_date} to {end_date} into {EXTRACTED_TABLES_DIR}"
    )
    return saved_files


def extract_cameo_codes(cameo_codes_url=CAMEO_CODES_URL, cameo_codes_path=CAMEO_CODES_PATH):
    logger.info("Collecting cameo codefile")
    codes_file = save_remote_file(cameo_codes_url, cameo_codes_path)
    return codes_file


def extract_gcam_codes(gcam_codes_url=GCAM_CODES_URL, gcam_codes_path=GCAM_CODES_PATH):
    logger.info("Collecting GCAM codefile")
    codes_file = save_remote_file(gcam_codes_url, gcam_codes_path)
    return codes_file


def extract_events_header():
    header_csvs = {
        "export.csv": f"{HEADERS_URL}GDELT_2.0_Events_Column_Labels_Header_Row_Sep2016.csv",
        "mentions.tsv": f"{HEADERS_URL}GDELT_2.0_eventMentions_Column_Labels_Header_Row_Sep2016.tsv",
        "gkg.tsv": f"{HEADERS_URL}GDELT_2.0_gdeltKnowledgeGraph_Column_Labels_Header_Row_Sep2016.tsv"
    }
    logger.info(f"Collecting header data: {header_csvs.values()}")
    for csv_name in header_csvs:
        url = header_csvs[csv_name]
        save_remote_file(url, os.path.join(HEADERS_DIR, csv_name))
    return header_csvs


if __name__ == '__main__':
    # extract_gdelt_csvs()
    extract_events_header()
