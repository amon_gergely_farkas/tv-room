import os

from src.etl import INTERMEDIATE_DATA_DIR

EXTRACTED_DATA_DIR = os.path.join(INTERMEDIATE_DATA_DIR, "extracted")
