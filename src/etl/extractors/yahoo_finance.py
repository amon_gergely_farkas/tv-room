from pandas_datareader import data as pdr
import yfinance as yf

from src.etl import DATA_CACHE_SESSION


def get_yahoo_data(ticker, start, end, *args, **kwargs):
    yf.pdr_override()
    return pdr.get_data_yahoo(ticker, start, end, session=DATA_CACHE_SESSION, *args, **kwargs)

