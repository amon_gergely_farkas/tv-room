import datetime as dt
import glob
import os

import pandas as pd
from logzero import logger

from src.etl.extractors.gdelt import CAMEO_CODES_PATH, EXTRACTED_TABLES_DIR, HEADERS_DIR, GDELT_FILE_DATE_FORMAT
from src.helpers import default_date_range


def transform_headers(table_name):
    header_csv_path = glob.glob(os.path.join(HEADERS_DIR, f"{table_name}.*"))
    if not header_csv_path:
        logger.error(f"Header not found for {table_name}")
        raise FileNotFoundError(f"Header not found for {table_name}")
    header_csv_path = header_csv_path[0]
    if header_csv_path.endswith(".csv"):
        header_df = pd.read_csv(header_csv_path)
    else:
        header_df = pd.read_csv(header_csv_path, delimiter="\t", usecols=['tableId', 'dataType', 'Description'])
        header_df.columns = ['tableId', 'dataType', 'Description']
    header_df = header_df["tableId"].values
    return header_df


@default_date_range()
def clean_table(table_name, start_date=None, end_date=None):
    logger.info(f"Transforming table {table_name} from {start_date} to {end_date}")
    shards = [f for f in glob.glob(EXTRACTED_TABLES_DIR + f"/{table_name}/*.zip")]
    logger.debug(f"Found {len(shards)} shards")

    headers = transform_headers(table_name)
    dfs = []
    for shard in shards:
        # Shard path basename looks like this: 20200501163000.csv.zip
        timestamp = dt.datetime.strptime(os.path.basename(shard).split(".")[0], GDELT_FILE_DATE_FORMAT)
        if not start_date <= timestamp <= end_date:
            continue
        day = timestamp.strftime("%Y%m%d")
        shard_df = pd.read_csv(shard, delimiter='\t', encoding="ISO-8859-1", names=headers, parse_dates=["DATE"])
        shard_df = shard_df.assign(day=day).reset_index()
        if "index" in shard_df.keys():
            del shard_df["index"]
        dfs.append(shard_df)

    cleaned_df = pd.concat(dfs) if len(dfs) else None
    return cleaned_df


@default_date_range()
def transform_gkg_table(start_date=None, end_date=None):
    logger.info("Transforming gkg table")
    gkg_df = clean_table("gkg", start_date=start_date, end_date=end_date)
    if not isinstance(gkg_df, pd.DataFrame) or not len(gkg_df):
        return
    # Extract article title if available in the extras
    page_title = gkg_df["Extras"].str.extract(r"<PAGE_TITLE>(.*)<\/PAGE_TITLE>")
    gkg_df["page_title"] = page_title
    # Only keep relevant columns
    wanted_cols = [
        "GKGRECORDID", "DATE", "SourceCommonName", "DocumentIdentifier",
        "V2Themes", "V2Persons", "V2Organizations", "V2Tone",
        "SharingImage", "SocialVideoEmbeds", "AllNames", "Amounts",
        "TranslationInfo", "Quotations", "day", "page_title"
    ]
    df = gkg_df[wanted_cols]
    # Rename columns
    df = df.rename(columns={
        "GKGRECORDID": "id",
        "DATE": "date",
        "SourceCommonName": "source",
        "DocumentIdentifier": "document_id",
        "V2Counts": "counts",
        "V2Themes": "themes",
        "V2Persons": "persons",
        "V2Organizations": "organizations",
        "V2Tone": "tone",
        "SharingImage": "thumbnail",
        "SocialVideoEmbeds": "video_embeds",
        "AllNames": "names",
        "Amounts": "amounts",
        "TranslationInfo": "translation_info",
        "Extras": "extras",
        "Quotations": "quotes"
    })
    # Change all column types to "string"(will change later for other typed cols)
    # df[df.columns] = df[df.columns].astype(str)
    # Change index to the "id" col
    df = df.set_index("id")
    # Parse 'date' col
    df["date"] = pd.to_datetime(df["date"])
    # Extract numerical columns from "tone"
    tone_cols = ["tone", "positive_words_perc", "negative_words_perc",
                 "emotion", "activity", "pronouns_perc", "word_count"]
    df[tone_cols] = df["tone"].str.split(",", expand=True)
    df[tone_cols] = df[tone_cols].astype("float")
    # Refactor 'themes', 'persons' , 'names', 'quotes' and 'amounts'
    cols_to_refactor = ["themes", "persons", "organizations", "names"]
    df[cols_to_refactor] = df[cols_to_refactor].astype("string").applymap(
        lambda x: list(map(lambda y: y.split(",")[0] if not pd.isna(y) else "", x.split(";") if not pd.isna(x) else "")
                       ))
    df["quotes"] = df["quotes"].astype("string").apply(
        lambda x: list(map(lambda y: y.split("|")[-1] if not pd.isna(y) else "", x.split("#") if not pd.isna(x) else "")
                       ))
    str_cols = ["source", "document_id", "thumbnail", "video_embeds", "page_title"]
    df[str_cols] = df[str_cols].astype("string")
    df.dropna(how="any", subset=["document_id"], inplace=True)
    return df


def transform_cameo_codes():
    cameo_codes_df = pd.read_csv(
        CAMEO_CODES_PATH,
        sep=':',
        header=None,
        dtype={'cameoCode': 'str'},
        names=['cameoCode', 'Description'],
        skiprows=2
    )
    cameo_codes_df['GoldsteinScale'] = cameo_codes_df.Description.apply(
        lambda x: pd.Series(x.split(']')[0].strip('[]'), name='GoldsteinScale')
    )
    cameo_codes_df['Description'] = cameo_codes_df.Description.apply(
        lambda x: pd.Series(x.split(']')[1], name='cameoCodeDescription')
    )

    cameo_codes_df.cameoCode = cameo_codes_df.cameoCode.apply(lambda x: x.strip())
    cameo_codes_df.set_index(cameo_codes_df.cameoCode.values, inplace=True)
    cameo_codes_df.index.rename('cameoCode', inplace=True)
    return cameo_codes_df


if __name__ == '__main__':
    # headers = transform_headers("export")

    df = transform_gkg_table()
