import os

import pandas as pd

from src.etl.extractors.gdelt import extract_events_header, extract_gdelt_csvs
from src import logger
from src.etl import TRANSFORMED_FILES_DIR
from src.etl.transformers.gdelt import transform_gkg_table
from src.helpers import default_date_range, create_dir_if_not_exists

TABLES_DIR = os.path.join(TRANSFORMED_FILES_DIR, "gdelt_tables")
PART_FREQ = "1H"  # https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases


@default_date_range()
def load_gkg_to_parquets(start_date=None, end_date=None, update_partitions=False):
    logger.info(f"Processing table gkg from {start_date.isoformat()} to {end_date.isoformat()}")
    table_parquet_dir = os.path.join(TABLES_DIR, "gkg")
    create_dir_if_not_exists(table_parquet_dir)
    partition_dates = pd.date_range(start=start_date, end=end_date, freq=PART_FREQ).to_pydatetime().tolist()
    logger.debug(f"{len(partition_dates)} partitions will be created")
    if len(partition_dates) < 2:
        logger.warning(
            f"No gkg partition could be generated for {start_date} - {end_date} with the frequency of {PART_FREQ}"
        )
        return
    last_date = partition_dates[0].replace(minute=0, second=0, microsecond=0)
    for partition_date in partition_dates[1:]:
        partition_date = partition_date.replace(minute=0, second=0, microsecond=0)
        partition_date_iso = partition_date.isoformat()
        partition_path = os.path.join(table_parquet_dir, partition_date_iso + ".parquet")
        if os.path.exists(partition_path) and partition_date != partition_dates[-1]:
            logger.debug(f"Found partition file for {partition_date_iso}")
            if not update_partitions:
                logger.debug("Skipping!")
                continue
        logger.debug(f"Loading {partition_date_iso} into {partition_path}")
        partition = transform_gkg_table(start_date=last_date, end_date=partition_date)
        if not isinstance(partition, pd.DataFrame) or not len(partition):
            logger.debug(f"Empty partition for {partition_date_iso}")
            continue
        partition.reset_index().to_parquet(partition_path)
        last_date = partition_date
    logger.info(f"{len(partition_dates)} partitions were generated")


if __name__ == '__main__':
    extract_events_header()
    extract_gdelt_csvs()
    # load_gkg_to_dir(update_partitions=True)
    load_gkg_to_parquets(update_partitions=False)
