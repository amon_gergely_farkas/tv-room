from src.etl.loaders.gdelt import load_gkg_to_parquets
from src.etl.extractors.gdelt import *


def collect_gdelt_data_for_yesterday():
    extract_gdelt_csvs()
    extract_cameo_codes()
    extract_gcam_codes()
    extract_events_header()


if __name__ == "__main__":
    collect_gdelt_data_for_yesterday()
    # df = transform_gkg_table()
    load_gkg_to_parquets(update_partitions=True)
