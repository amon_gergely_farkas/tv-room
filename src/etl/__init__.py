import datetime
import os

import requests_cache

from src import ROOT_DIR
from src.helpers import create_dir_if_not_exists

# define directory structure for the etl pipelines
DATA_DIR = os.path.join(ROOT_DIR, "data")
CACHE_DIR = os.path.join(DATA_DIR, "cache")
INTERMEDIATE_DATA_DIR = os.path.join(ROOT_DIR, DATA_DIR, "intermediate")
TRANSFORMED_FILES_DIR = os.path.join(INTERMEDIATE_DATA_DIR, "transformed")

GDELT_TABLES = ("export", "mentions", "gkg")

create_dir_if_not_exists(CACHE_DIR)
DATA_CACHE_EXPIRE_AFTER = datetime.timedelta(days=3)
DATA_CACHE_SESSION = requests_cache.CachedSession(
    cache_name=os.path.join(CACHE_DIR, "requests_cache"),
    backend='sqlite',
    expire_after=DATA_CACHE_EXPIRE_AFTER
)

