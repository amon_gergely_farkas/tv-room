import functools
import os
import urllib.request
from logzero import logger
import datetime as dt
import itertools


def create_dir_if_not_exists(dir_path):
    if not os.path.exists(dir_path):
        os.makedirs(dir_path, exist_ok=True)
        logger.debug(f"Created new directory at {dir_path}")
    return dir_path


def create_dirs_if_not_exists(*dirs):
    return [create_dir_if_not_exists(dirpath) for dirpath in dirs]


def save_remote_file(url, save_to, override_old=False):
    if os.path.exists(save_to) and not override_old:
        return save_to
    create_dir_if_not_exists(os.path.dirname(save_to))
    urllib.request.urlretrieve(url, save_to)
    return save_to


def default_date_range(date_range=dt.timedelta(days=1)):
    def _(function):
        @functools.wraps(function)
        def wrap(*args, **kwargs):
            start_date, end_date = kwargs.get("start_date"), kwargs.get("end_date")
            if not end_date:
                end_date = dt.datetime.now().replace(second=0, microsecond=0)
                logger.debug(f"defaulting end_date to {end_date}")
            if not start_date:
                start_date = (end_date - date_range).replace(second=0, microsecond=0)
                logger.debug(f"defaulting start_date to {start_date}")
            kwargs.update({"start_date": start_date, "end_date": end_date})
            return function(*args, **kwargs)

        return wrap

    return _


def day_range(start, end, intv):
    start = start.replace(hour=0, minute=0, second=0, microsecond=0)
    end = end.replace(hour=0, minute=0, second=0, microsecond=0)

    diff = (end - start) / intv
    for i in range(intv):
        yield start + diff * i
    yield end


def grouper(n, iterable, fillvalue=None):
    "grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return itertools.zip_longest(*args, fillvalue=fillvalue)


if __name__ == '__main__':
    import datetime as dt

    print(list(day_range(dt.datetime.now() - dt.timedelta(days=1), dt.datetime.now(), 10)))
